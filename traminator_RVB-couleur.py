#!/usr/bin/env python3

"""
Transformation d'image matricielle vers une image matricielle avec un
une trnaformation d'un pixel en un elements reprentant la "couleur" du
pixel de l'image de depart
 - Il est recommander d'utilise l'arborescence suivante :  
        └── Images
            ├── 00_Test             : lot d'images de tests
            ├── 10_Sources          : lot d'images d'entrée
            └── 20_Destination      : lot d'images de sortie
 
"""

__author__ = "Laurent Berthelot"
__license__ = "GPL"
__credits__ = ["Laurent Berthelot", "Zoe Berthelot"]
__version__ = "0.0.0"
__maintainer__ = "LAurent_B"
__email__ = "LAurent_B@gmx.fr"
__status__ = "Development"

import os
from PIL import Image, ImageDraw
import time

# Configuration d'usage
BATCH_MODE = False              # False ou True

IMAGE_ENTREE = 'chaton-couleur.jpg'
IMAGE_FORMAT_ENTREE = 'jpg'
IMAGE_REPERTOIRE_ENTREE = 'Images/00_Test'

IMAGE_FORMAT_SORTIE = 'jpg'
IMAGE_REPERTOIRE_SORTIE = 'Images/20_Destination'
IMAGE_FOND_SORTIE = 'yellow'
IMAGE_POINT_SORTIE = 'orange'
IMAGE_POINT_C1 = 'pink'
IMAGE_POINT_C2 = 'cyan'
IMAGE_POINT_C3 = 'brown'

IMAGE_MODE_SORTIE = 'RGB'                   # Pas toucher pour l'instant

# ARBITRIARE - AJUSTAGE HUMAIN
QUADRILLAGE = 10
SCALE = 6
SCALE_DOT = 1.5


# Du pour fait joli a l'ecran
SEPARATEUR1 = "########################################################"

########################################################################
# FONCTIONS
########################################################################

def liste_images_entrees(repertoire, ext_format):
    """
    Recupertation de la liste des images ayant l'extension "ext_format"
    dans le repertoire "repertoire"
    """
    liste_image = []
    print(' Lecture des images {} dansle repertoire {}'.format(ext_format, repertoire))
    chemin = os.path.abspath(repertoire)
    for fichier in os.listdir(chemin):
        if fichier.endswith(ext_format):
            # Prints only text file present in My Folder
            print('    * {}'.format(fichier))
            liste_image.append(fichier)
    
    return liste_image

def lecture_image(repertoire, image_entree):
    """
    Lecture d'un image d'entre
    """
    ch = os.path.join(repertoire, image_entree)
    chemin = os.path.abspath(ch)
    im = Image.open(chemin)
    im_dim_x, im_dim_y = im.size
    # print(SEPARATEUR1)
    # print('INFORMATION DE L IMAGE')
    print('    * Format', im.format)
    print('    * Mode  ', im.mode)
    print('    * Dimensions ligne {} colonne {} '.format(im_dim_y, im_dim_x))
    # im.show()
    im_nb = im.convert('L')
    #im_nb.show()
    return im

def sauvegarde_image(image_sortie, repertoire, nom_image_sortie):
    """
    Sauvegarde d'un image de sortie
    """
    ch = os.path.join(repertoire, nom_image_sortie)
    chemin = os.path.abspath(ch)
    image_sortie.save(chemin)

def creation_image(mode, width, height, scale, couleur):
    """
    Creation du fond d'image
        en mode            : Mode
        de dimension       : l = width*scale, h = height*scale
        de couleur de fond : couleur
    """
    im = Image.new(mode, (width*scale, height*scale), couleur)
    imagedraw = ImageDraw.Draw(im)

    return im, imagedraw

def motif_couleurs(x_motif, y_motif, dim_carre, image):
    r, g, b = 0, 0, 0
    nb_pixels = 0
    for x_pixel in range(x_motif, x_motif + dim_carre):
        for y_pixel in range(y_motif, y_motif + dim_carre):
            nb_pixels += 1
            p = image.getpixel( (x_pixel, y_pixel) )
            r+= p[0]
            g+= p[1]
            b+= p[2]
    r /= nb_pixels
    g /= nb_pixels
    b /= nb_pixels
    v = (r + g + b)/3       # couleur moyenne
    c = (r, g, b, v)
    
    return c

def motif_circle(imagedraw, x, y, r, scale, couleur):
    """
    Dessinage d'un cercle centre x, y et de rayon r dans l'image image
    """
    imagedraw.ellipse((scale*(x-r / 2 ), scale*(y-r / 2), scale*(x+r), scale*(y+r) ), fill = couleur)

def redimension_image(im_entree, dim_carre):

    w, h = im_entree.size

    block_w = int( w / dim_carre )
    width = block_w * dim_carre
    block_h = int( h / dim_carre )
    height = block_h * dim_carre
    
    print( w, h, width, height, dim_carre)
    
    xmin = int((w - width ) / 2)
    ymin = int((h - height) / 2)
    
    # Decoupage de l'image avec centrage
    im_sortie = im_entree.crop((xmin, ymin, xmin + width, ymin + height))
    
    print(im_sortie.size)
    
    return im_sortie

def point_couleur(image, intensite, x_pos, y_pos, couleur):
    intensite_c = 1 - intensite / 255
    rayon_r = QUADRILLAGE / 2 * intensite_c * SCALE_DOT
    motif_circle(image, x_pos, y_pos, rayon_r, SCALE, couleur)


def traitement_image_trichrome(im_entree):
    """
    Conversion image source vers image tramisée
    """
    print('Traitement')
    
    # redimansionement image avec la taille du QUADRILLAGE
    im_traitement = redimension_image(im_entree, QUADRILLAGE)

    # generation fond image sortie
    mode = 'RGB' # 'RGB' ou 'CMYK'
    nb_colonnes, nb_lignes = im_traitement.size
    # im_traitement.show()
    # print(im_traitement.size)
    im_sortie, image_sortiedraw = creation_image(IMAGE_MODE_SORTIE, 
                                                 nb_colonnes, nb_lignes,
                                                 SCALE,
                                                 IMAGE_FOND_SORTIE)

    # parcours lignes colonnes
    for ligne in range(0, nb_lignes, QUADRILLAGE):
        for colonne in range(0, nb_colonnes, QUADRILLAGE):
            pass
            intensites = motif_couleurs(colonne, ligne, QUADRILLAGE, im_traitement)
            print('intensites :', intensites)
            
            # point rouge
            x_circle_r = colonne + 4 * QUADRILLAGE /3
            y_circle_r = ligne 
            """
            intensite_r = 1 - intensites[0] / 255
            rayon_r = QUADRILLAGE / 2 * intensite_r * SCALE_DOT
            motif_circle(image_sortiedraw, x_circle_r, y_circle_r, rayon_r, SCALE, 'red')
            """
            point_couleur(image_sortiedraw, intensites[0], x_circle_r, y_circle_r, IMAGE_POINT_C1)
            
            
            # point vert
            x_circle_v = colonne + 5 * QUADRILLAGE /3
            y_circle_v = ligne  + QUADRILLAGE /3
            """
            intensite_v = 1 - intensites[1] / 255
            rayon_v = QUADRILLAGE / 2 * intensite_v * SCALE_DOT
            motif_circle(image_sortiedraw, x_circle_v, y_circle_v, rayon_v, SCALE, 'green')
            """
            point_couleur(image_sortiedraw, intensites[1], x_circle_v, y_circle_v, IMAGE_POINT_C2)
            
            # point bleu
            x_circle_b = colonne + 6 * QUADRILLAGE /3
            y_circle_b = ligne  + 2 * QUADRILLAGE /3
            """
            intensite_b = 1 - intensites[2] / 255
            rayon_b = QUADRILLAGE / 2 * intensite_b * SCALE_DOT
            motif_circle(image_sortiedraw, x_circle_b, y_circle_b, rayon_b, SCALE, 'blue')
            """
            point_couleur(image_sortiedraw, intensites[2], x_circle_b, y_circle_b, IMAGE_POINT_C3)

    return im_sortie

########################################################################
# PROGRAMME PRINCIPALE
########################################################################
if __name__ == "__main__":
    print(SEPARATEUR1)
    print('Hello World : TramInator')

    chemin_courant = os.getcwd()

    # preparation des trucs en entrees
    images_entrees = []
    print(SEPARATEUR1)    
    print('Creation de la liste d image a traiter')
    print('   * BATCH_MODE : {}'.format(BATCH_MODE))

    if BATCH_MODE:
        images_entrees = liste_images_entrees(IMAGE_REPERTOIRE_ENTREE, IMAGE_FORMAT_ENTREE)
    else:
        images_entrees.append(IMAGE_ENTREE)
    print('   * Les images : {}'.format(images_entrees))

    # traitement de la liste d'image
    print(SEPARATEUR1)
    print('Traitement de la liste d image')
    for fichier_image in images_entrees:
        print(SEPARATEUR1)
        print('   * Image en cours : {}'.format(fichier_image))
        image_source = lecture_image(IMAGE_REPERTOIRE_ENTREE, fichier_image)

        image_sortie = traitement_image_trichrome(image_source)
    
        # image_sortie = image_sortie.rotate(270, expand = 1)
    
        sauvegarde_image(image_sortie, IMAGE_REPERTOIRE_SORTIE, fichier_image)

    print(SEPARATEUR1)
    print('Goodbye World : TramInator')
    print(SEPARATEUR1)

########################################################################
# REFERENCES
########################################################################
# https://stackoverflow.com/questions/14088375/how-can-i-convert-rgb-to-cmyk-and-vice-versa-in-python
