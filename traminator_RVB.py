#!/usr/bin/env python3

"""
Transformation d'image matricielle vers une image matricielle avec un
une trnaformation d'un pixel en un elements reprentant la "couleur" du
pixel de l'image de depart
 - Il est recommander d'utilise l'arborescence suivante :
        └── Images
            ├── 00_Test             : lot d'images de tests
            ├── 10_Sources          : lot d'images d'entrée
            └── 20_Destination      : lot d'images de sortie
"""

__author__ = "Laurent Berthelot"
__license__ = "GPL"
__credits__ = ["Laurent Berthelot", "Zoe Berthelot"]
__version__ = "0.0.0"
__maintainer__ = "LAurent_B"
__email__ = "LAurent_B@gmx.fr"
__status__ = "Development"

import os
import time
from PIL import Image, ImageDraw


########################################################################
# Configuration d'usage
BATCH_MODE = True              # False ou True
COULEUR_MODE = '4'              # '1' : MONO / '3' : TRI / '4' : QUADRI

########################################################################
# Gestion des sources / format images
IMAGE_ENTREE = 'chaton-couleur.jpg'
IMAGE_FORMAT_ENTREE = 'jpg'
IMAGE_REPERTOIRE_ENTREE = 'Images/00_Test'

# Gestion de la destination / format images
IMAGE_FORMAT_SORTIE = 'jpg'
IMAGE_REPERTOIRE_SORTIE = 'Images/20_Destination'

########################################################################
# Gestion des couleurs
IMAGE_FOND_SORTIE = 'white'

# Couleur pour monochromie
IMAGE_POINT_SORTIE = 'black'

# Couleur pour trichromie
IMAGE_POINT_C1 = 'pink'
IMAGE_POINT_C2 = 'cyan'
IMAGE_POINT_C3 = 'brown'

# Couleur pour quadrichromie
IMAGE_POINT_CQ1 = 'cyan'
IMAGE_POINT_CQ2 = 'magenta'
IMAGE_POINT_CQ3 = 'yellow'
IMAGE_POINT_CQ4 = 'black'

IMAGE_MODE_SORTIE = 'RGB'                   # Pas toucher pour l'instant

########################################################################
# ARBITRIARE - AJUSTAGE HUMAIN
QUADRILLAGE = 10
SCALE = 6
SCALE_DOT = 1.5

########################################################################
prefixe = IMAGE_FOND_SORTIE + '_' + IMAGE_POINT_SORTIE + '_' + \
         IMAGE_MODE_SORTIE + '_' + str(QUADRILLAGE) + '_' + \
         str(SCALE) + '_' +  str(SCALE_DOT).replace(".", "p") + '_'

# Du pour fait joli a l'ecran
SEPARATEUR1 = "########################################################"

########################################################################
# FONCTIONS
########################################################################

def liste_images_entrees(repertoire, ext_format):
    """
    Recupertation de la liste des images ayant l'extension "ext_format"
    dans le repertoire "repertoire"
    """
    liste_image = []
    print(' Lecture des images {} dansle repertoire {}'.format(ext_format, repertoire))
    chemin = os.path.abspath(repertoire)
    for fichier in os.listdir(chemin):
        if fichier.endswith(ext_format):
            # Prints only text file present in My Folder
            print('    * {}'.format(fichier))
            liste_image.append(fichier)

    return liste_image

def lecture_image(repertoire, image_entree):
    """
    Lecture d'un image d'entre
    """
    ch = os.path.join(repertoire, image_entree)
    chemin = os.path.abspath(ch)
    im = Image.open(chemin)
    im_dim_x, im_dim_y = im.size
    # print(SEPARATEUR1)
    # print('INFORMATION DE L IMAGE')
    print('    * Format', im.format)
    print('    * Mode  ', im.mode)
    print('    * Dimensions ligne {} colonne {} '.format(im_dim_y, im_dim_x))
    # im.show()
    im_nb = im.convert('L')
    #im_nb.show()
    return im

def sauvegarde_image(image_sortie, repertoire, nom_image_sortie):
    """
    Sauvegarde d'un image de sortie
    """
    ch = os.path.join(repertoire, nom_image_sortie)
    chemin = os.path.abspath(ch)
    image_sortie.save(chemin)

def creation_image(mode, width, height, scale, couleur):
    """
    Creation du fond d'image
        en mode            : Mode
        de dimension       : l = width*scale, h = height*scale
        de couleur de fond : couleur
    """
    im = Image.new(mode, (width*scale, height*scale), couleur)
    imagedraw = ImageDraw.Draw(im)

    return im, imagedraw

def motif_couleurs(x_motif, y_motif, dim_carre, image):
    """
    Recuperation des intensite dans le motif QUADRILLAGE
        Gestion si image RVB ou CMYK
    """
    nb_pixels = 0

    if image.mode == 'RGB':
        r, g, b = 0, 0, 0

        for x_pixel in range(x_motif, x_motif + dim_carre):
            for y_pixel in range(y_motif, y_motif + dim_carre):
                nb_pixels += 1
                p = image.getpixel( (x_pixel, y_pixel) )
                r+= p[0]
                g+= p[1]
                b+= p[2]
        r /= nb_pixels
        g /= nb_pixels
        b /= nb_pixels
        v = (r + g + b)/3       # couleur moyenne
        c = (r, g, b, v)

    elif image.mode == 'CMYK':
        c, m, y, k = 0, 0, 0, 0

        for x_pixel in range(x_motif, x_motif + dim_carre):
            for y_pixel in range(y_motif, y_motif + dim_carre):
                nb_pixels += 1
                p = image.getpixel( (x_pixel, y_pixel) )
                c+= p[0]
                m+= p[1]
                y+= p[2]
                k+= p[3]
        c /= nb_pixels
        m /= nb_pixels
        y /= nb_pixels
        k /= nb_pixels
        v = (c + m + y + k)/4       # couleur moyenne
        c = (c, m, y, k, v)

    return c

def motif_circle(imagedraw, x, y, r, scale, couleur):
    """
    Dessinage d'un cercle centre x, y et de rayon r dans l'image image
    """
    imagedraw.ellipse((scale*(x-r / 2 ),
                       scale*(y-r / 2),
                       scale*(x+r),
                       scale*(y+r) ),
                       fill = couleur)

def redimension_image(im_entree, dim_carre):
    """
    Reajuste la dimension de l'image en multiple de QUADRILLAGE
        recentre la zone de redecoupage
    """
    w, h = im_entree.size

    block_w = int( w / dim_carre )
    width = block_w * dim_carre
    block_h = int( h / dim_carre )
    height = block_h * dim_carre

    print( w, h, width, height, dim_carre)

    xmin = int((w - width ) / 2)
    ymin = int((h - height) / 2)

    # Decoupage de l'image avec centrage
    im_sortie = im_entree.crop((xmin, ymin, xmin + width, ymin + height))

    print(im_sortie.size)

    return im_sortie

def point_couleur(image, intensite, x_pos, y_pos, couleur):
    """
    Desine le cercle de
        de centre x_pos y_pos
        de rayon = f(intensite)
        couleur 'couleur'
        dans image : image
    """
    intensite_c = 1 - intensite / 255
    rayon_r = QUADRILLAGE / 2 * intensite_c * SCALE_DOT
    motif_circle(image, x_pos, y_pos, rayon_r, SCALE, couleur)

def traitement_image_quadrichrome(im_entree):
    """
    Conversion image source quadrichrominque vers image tramisée
    """
    print('Traitement')

    # redimansionement image avec la taille du QUADRILLAGE
    im_traitement = redimension_image(im_entree, QUADRILLAGE)

    # generation fond image sortie
    mode = 'RGB' # 'RGB' ou 'CMYK'
    nb_colonnes, nb_lignes = im_traitement.size
    # im_traitement.show()
    # print(im_traitement.size)
    im_sortie, image_sortiedraw = creation_image(IMAGE_MODE_SORTIE,
                                                 nb_colonnes, nb_lignes,
                                                 SCALE,
                                                 IMAGE_FOND_SORTIE)

    # parcours lignes colonnes
    for ligne in range(0, nb_lignes, QUADRILLAGE):
        for colonne in range(0, nb_colonnes, QUADRILLAGE):
            intensites = motif_couleurs(colonne, ligne, QUADRILLAGE, im_traitement)
            # print('intensites :', intensites)

            # point cyan
            x_circle_r = colonne + 4 * QUADRILLAGE /3
            y_circle_r = ligne
            point_couleur(image_sortiedraw, intensites[0], x_circle_r, y_circle_r, IMAGE_POINT_CQ1)

            # point magneta
            x_circle_v = colonne + 5 * QUADRILLAGE /3
            y_circle_v = ligne
            point_couleur(image_sortiedraw, intensites[1], x_circle_v, y_circle_v, IMAGE_POINT_CQ2)

            # point yellow
            x_circle_b = colonne + 4 * QUADRILLAGE /3
            y_circle_b = ligne  + 2 * QUADRILLAGE /3
            point_couleur(image_sortiedraw, intensites[2], x_circle_b, y_circle_b, IMAGE_POINT_CQ3)

            # point black
            x_circle_b = colonne + 5 * QUADRILLAGE /3
            y_circle_b = ligne  + 2 * QUADRILLAGE /3
            point_couleur(image_sortiedraw, intensites[3], x_circle_b, y_circle_b, IMAGE_POINT_CQ4)

    return im_sortie

def traitement_image_trichrome(im_entree):
    """
    Conversion image source trichromique vers image tramisée
    """
    print('Traitement')

    # redimansionement image avec la taille du QUADRILLAGE
    im_traitement = redimension_image(im_entree, QUADRILLAGE)

    # generation fond image sortie
    mode = 'RGB' # 'RGB' ou 'CMYK'
    nb_colonnes, nb_lignes = im_traitement.size
    # im_traitement.show()
    # print(im_traitement.size)
    im_sortie, image_sortiedraw = creation_image(IMAGE_MODE_SORTIE,
                                                 nb_colonnes, nb_lignes,
                                                 SCALE,
                                                 IMAGE_FOND_SORTIE)

    # parcours lignes colonnes
    for ligne in range(0, nb_lignes, QUADRILLAGE):
        for colonne in range(0, nb_colonnes, QUADRILLAGE):
            intensites = motif_couleurs(colonne, ligne, QUADRILLAGE, im_traitement)
            # print('intensites :', intensites)

            # point rouge
            x_circle_r = colonne + 4 * QUADRILLAGE /3
            y_circle_r = ligne
            point_couleur(image_sortiedraw, intensites[0], x_circle_r, y_circle_r, IMAGE_POINT_C1)

            # point vert
            x_circle_v = colonne + 5 * QUADRILLAGE /3
            y_circle_v = ligne  + QUADRILLAGE /3
            point_couleur(image_sortiedraw, intensites[1], x_circle_v, y_circle_v, IMAGE_POINT_C2)

            # point bleu
            x_circle_b = colonne + 6 * QUADRILLAGE /3
            y_circle_b = ligne  + 2 * QUADRILLAGE /3
            point_couleur(image_sortiedraw, intensites[2], x_circle_b, y_circle_b, IMAGE_POINT_C3)

    return im_sortie

def traitement_image_monochrome(im_entree):
    """
    Conversion image source RVB vers image tramisée monocouleur
    """
    print('Traitement')

    # redimansionement image avec la taille du QUADRILLAGE
    im_traitement = redimension_image(im_entree, QUADRILLAGE)

    # generation fond image sortie
    mode = 'RGB' # 'RGB' ou 'CMYK'
    nb_colonnes, nb_lignes = im_traitement.size
    # im_traitement.show()
    # print(im_traitement.size)
    im_sortie, image_sortiedraw = creation_image(IMAGE_MODE_SORTIE,
                                                 nb_colonnes, nb_lignes,
                                                 SCALE,
                                                 IMAGE_FOND_SORTIE)

    # parcours lignes colonnes
    for ligne in range(0, nb_lignes, QUADRILLAGE):
        for colonne in range(0, nb_colonnes, QUADRILLAGE):
            # pass
            intensites = motif_couleurs(colonne, ligne, QUADRILLAGE, im_traitement)
            x_circle = colonne + QUADRILLAGE
            y_circle = ligne
            point_couleur(image_sortiedraw, intensites[3], x_circle, y_circle, IMAGE_POINT_SORTIE)

    return im_sortie

########################################################################
# PROGRAMME PRINCIPALE
########################################################################
if __name__ == "__main__":
    print(SEPARATEUR1)
    print('Hello World : TramInator')

    chemin_courant = os.getcwd()

    # preparation des trucs en entrees
    images_entrees = []
    print(SEPARATEUR1)
    print('Creation de la liste d image a traiter')
    print('   * BATCH_MODE : {}'.format(BATCH_MODE))

    if BATCH_MODE:
        images_entrees = liste_images_entrees(IMAGE_REPERTOIRE_ENTREE, IMAGE_FORMAT_ENTREE)
    else:
        images_entrees.append(IMAGE_ENTREE)
    print('   * Les images : {}'.format(images_entrees))

    # traitement de la liste d'image
    print(SEPARATEUR1)

    if COULEUR_MODE == '1':
        prefixe = 'mono_' + prefixe
    elif COULEUR_MODE == '3':
        prefixe = 'tri_' + prefixe
    elif COULEUR_MODE == '4':
        prefixe = 'quadri_' + prefixe
    else:
        print('ca existe pas')

    print('Traitement de la liste d image')
    for fichier_image in images_entrees:
        print(SEPARATEUR1)
        print('   * Image en cours : {}'.format(fichier_image))
        image_source = lecture_image(IMAGE_REPERTOIRE_ENTREE, fichier_image)

        if COULEUR_MODE == '1':
            image_sortie = traitement_image_monochrome(image_source)
        elif COULEUR_MODE == '3':
            image_sortie = traitement_image_trichrome(image_source)
        elif COULEUR_MODE == '4':
            cmyk_image = image_source.convert('CMYK')
            image_sortie = traitement_image_quadrichrome(cmyk_image)
        else:
            print('ca existe pas')

        # image_sortie = image_sortie.rotate(270, expand = 1)

        sauvegarde_image(image_sortie, IMAGE_REPERTOIRE_SORTIE, prefixe + fichier_image)

    print(SEPARATEUR1)
    print('Goodbye World : TramInator')
    print(SEPARATEUR1)

########################################################################
# REFERENCES
########################################################################
# https://stackoverflow.com/questions/14088375/how-can-i-convert-rgb-to-cmyk-and-vice-versa-in-python
